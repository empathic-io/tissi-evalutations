Meteor.startup(function() {
	User.init();

	App.subscribe('allDays', 'all-days');
	App.subscribe('allQuestions', 'all-questions');
});
