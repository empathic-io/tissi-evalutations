EvaluationController = RouteController.extend({
	waitOn: function() {
		GAnalytics.pageview("/evaluate");
		GAnalytics.pageview(this.path);

		return App.commonSubscriptions()
			.concat([
				this.subscribe('evaluations-by-selector', {target: this.params['uid'], author: User.userId}),
				this.subscribe('summary-stats-by-selector', {target: this.params['uid']})
			]);
	},

	data: function() {
		if(!this.ready()) return {};

		var uid = this.params['uid'];
		var date = moment(uid.substr(1, 8), "YYYYMMDD").format("YYYY-MM-DD");

		var day = Days.findOne({date: date});
		if(!day) return null;

		return day.findElementByUid(uid);
	}
});
