HomeController = RouteController.extend({
	waitOn: function() {
		GAnalytics.pageview("/");

		return App.commonSubscriptions();
	},

	data: function() {
		return {};
	}
});