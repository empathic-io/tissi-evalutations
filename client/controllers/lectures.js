LecturesController = RouteController.extend({
	waitOn: function() {
		GAnalytics.pageview("/lectures");

		return App.commonSubscriptions();
	},

	data: function() {
		return {
            days: Days.find({}, {sort: {date: 1}})
		};
	}
});
