App = {

	_subscriptionsHandles: {},

	// list of heavy subscriptions, which should not be active for a whole time
	_subscriptionsBlacklist: [
		'activeDaySummaryStats'
	],

	subscribe: function(handleName, dataSetName, params) {
		if(typeof params != "undefined" && params != null)
			this._subscriptionsHandles[handleName] = Meteor.subscribe(dataSetName, params);
		else
			this._subscriptionsHandles[handleName] = Meteor.subscribe(dataSetName);
	},

	stopSubscription: function(handleName) {
		var handle = this.subscriptionHandle(handleName);
		if(handle) {
			handle.stop();
			delete this._subscriptionsHandles[handleName];
		}
	},

	stopBlacklistedSubscriptions: function() {
		_.forEach(this._subscriptionsBlacklist, function(handleName) {
			App.stopSubscription(handleName);
		});
	},

	subscriptionHandlesArray: function() {
		return $.map(this._subscriptionsHandles, function(value, index) {
			return [value];
		});
	},

	subscriptionHandle: function(handleName) {
		return this._subscriptionsHandles[handleName]
	},

	commonSubscriptions: function() {
		this.stopBlacklistedSubscriptions();
		return this.subscriptionHandlesArray();
	}
};