User = {

	userId: null,

	_generateId: function() {
		return Math.round((Math.random() * 1000000) + 100000).toString();
	},

	init: function() {
		this.userId = Cookie.get('VocoUserId');
		if(!this.userId) {
			this.userId = this._generateId();
			Cookie.set('VocoUserId', this.userId, { expires: 5*365 });
		}

		GAnalytics.event("user", "login", "userId", this.userId);
	},

	evaluate: function(evaluation) {
		var previousEvaluation = Evaluations.findOne({author: this.userId, target: evaluation.target});

		if(previousEvaluation) {
			Evaluations.update({_id: previousEvaluation._id}, {
				$set: {
					average: evaluation.average,
					answers: evaluation.answers,
					timestamp: parseInt(moment().format('X'))
				}
			});
		} else {
			evaluation.author = this.userId;
			evaluation.timestamp = parseInt(moment().format('X'));
			Evaluations.insert(evaluation);
		}

		var $indicator = $('#saving-indicator');
		$indicator.show();
		$indicator.fadeOut(1500);

		GAnalytics.event("user", "evaluated", "targetUid", evaluation.target);
	}
};