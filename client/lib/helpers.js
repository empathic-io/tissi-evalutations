UI.registerHelper('formatAsParagraphs', function(str) {
	var output = "";
	str = str || "";

	var lines = str.replace(/\r\n?/g, "\n").split(/\n\n+/g);
	for(var i = 0; i < lines.length; i++) {
		if(lines[i]) {
			output += '<p class="p-lg">' + lines[i].replace(/\n/g, '<br/>\n') + '</p>';
		}
	}

	return new Handlebars.SafeString(output);
});