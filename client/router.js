Router.onBeforeAction('dataNotFound');

Router.configure({
	notFoundTemplate: 'page404',
	layoutTemplate: 'layoutMain'
});

Router.map(function() {
    this.route('home', {
        path: '/',
        template: 'pageHome',
        controller: HomeController
    });

	this.route('lectures', {
		path: '/lectures',
		template: 'pageLectures',
		controller: LecturesController
	});

	this.route('evaluation', {
		path: '/evaluate/:uid',
		template: 'pageEvaluation',
		controller: EvaluationController
	});
});