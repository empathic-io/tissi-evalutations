Template.evaluationFeedback.events({

	'click #feedback-show-button': function(event, instance) {
		event.preventDefault();

		$('#feedback-show-button').hide();
		$('#feedback-input-row').show(500);

		return false;
	},

	'click #feedback-submit-button': function(event, instance) {
		event.preventDefault();

		var message = $('#feedback-input').val();
		if(message.length === 0) return false;

		FeedbackMessages.insert({
			author: User.userId,
			target: instance.data.uid,
			message: message,
			timestamp: parseInt(moment().format('X'))
		});

		$('#feedback-input-row').hide(500);

		var $indicator = $('#saving-indicator');
		$indicator.show();
		$indicator.fadeOut(1500);

		$('#feedback-success-row').show();

		return false;
	}

});

Template.evaluationDescription.events({

	'click #description-show-button': function(event, instance) {
		event.preventDefault();

		$('#description-row').toggle(500);

		return false;
	}

});