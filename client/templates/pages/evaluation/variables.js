Template.evaluationQuestion.previousAnswerValue = function(targetUid) {
	var evaluation = Evaluations.findOne({author: User.userId, target: targetUid}, {reactive: false});
	if(!evaluation) return 50;
	return (typeof evaluation.answers[this.uid] === "undefined" ? 50 : evaluation.answers[this.uid]);
};

Template.pageEvaluation.previousEvaluationAverage = function() {
	var evaluation = Evaluations.findOne({author: User.userId, target: this.uid}, {reactive: false});
	if(!evaluation) return 50;
	return evaluation.average;
};
