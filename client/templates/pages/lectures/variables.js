Template.pageLectures.formatDaysDate = function(date) {
	return moment(date, "YYYYMMDD").format("MMMM DD, YYYY");
};

Template.pageLectures.rendered = function() {
	if(!this.data || !this.data.days) {
		return;
	}

	var i = 0, activeDay = 0;
	var now = moment();

	this.data.days.forEach(function(day) {
		if(moment(day.date).isSame(now, 'day')) {
			activeDay = i;
		}
		i++;
	});

	$('.collapse').eq(activeDay).collapse('show');
};