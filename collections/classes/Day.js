Day = function(day) {
	_.extend(this, day);

	if(this.sections) {
		var _this = this;
		_.forEach(this.sections, function(section, index, sections) {
			sections[index] = new Section(section, index, _this.date);
		});
	}
};

_.extend(Day.prototype, {
	findElementByUid: function(uid) {
		var element = null;

		_.forEach(this.sections, function(section) {
			if(element != null) return;

			if(section.uid === uid) {
				element = section;
				return;
			}

			if(section.lectures) {
				_.forEach(section.lectures, function(lecture) {
					if(lecture.uid === uid) element = lecture;
				});
			}
		});

		return element;
	},

	getAllUids: function() {
		var uids = [];

		_.forEach(this.sections, function(section) {
			uids.push(section.uid);

			if(section.lectures) {
				_.forEach(section.lectures, function(lecture) {
					uids.push(lecture.uid);
				});
			}
		});

		return uids
	}
});
