EvaluableEntityPrototype = {
	_makeDateTime: function(date, time) {
		date = moment(date);
		var parts = time.split(':');

		date.hours(parts[0]);
		date.minutes(parts[1]);

		return date;
	},

	getStartTime: function() {
		var times = this.time.split(/\s+-\s+/);
		if(times.length == 0) {
			return null;
		}

		return this._makeDateTime(this.date, times[0]);
	},

	getEndTime: function() {
		var times = this.time.split(/\s+-\s+/);
		if(times.length < 2) {
			return null;
		}

		return this._makeDateTime(this.date, times[1]);
	},

	isNow: function() {
		var start = this.getStartTime().subtract('seconds', 1);
		var end = this.getEndTime() || start.clone().add('hours', 4);

		return start.isBefore() && end.isAfter();
	},

	canBeEvaluated: function() {
        //return !this.lectures && this.getQuestions() && this.getStartTime().isBefore();
		// evaluating disabled
		return this.sample;
	},

	getQuestions: function() {
		if(!this.questions) return null;

		var questions = Questions.find({uid: {$in: this.questions}}).fetch();
		return (questions.length === 0 ? null : questions)
	},

	getSummaryStats: function() {
		if(this.lectures) return null;
		return SummaryStats.findOne({target: this.uid}) || {
			schoolAverage: 50,
			evaluationsCount: 0
		};
	}
};
