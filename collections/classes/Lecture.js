Lecture = function(lecture, index, date, time) {
	_.extend(this, lecture);

	this.index = index;
	this.date = date;
	this.time = time;
	this.uid = "l" + (this.date + this.time + this.groups + this.index).replace(/[^\d]/g, '');
};

_.extend(Lecture.prototype, EvaluableEntityPrototype);