Section = function(section, index, date) {
	_.extend(this, section);

	this.date = date;
	this.index = index;
	this.uid = "s" + (this.date + this.time + this.index).replace(/[^\d]/g, '');

	if(this.lectures) {
		if(this.questions) this.questions = null;

		var _this = this;
		_.forEach(this.lectures, function(lecture, index, lectures) {
			lectures[index] = new Lecture(lecture, index, _this.date, _this.time);
		});
	}
};

_.extend(Section.prototype, EvaluableEntityPrototype);
