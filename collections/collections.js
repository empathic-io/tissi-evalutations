Days = new Meteor.Collection("Days", {
	transform: function(doc) { return new Day(doc); }
});

Evaluations = new Meteor.Collection("Evaluations");

Questions = new Meteor.Collection("Questions");

SummaryStats = new Meteor.Collection("SummaryStats");

FeedbackMessages = new Meteor.Collection("FeedbackMessages");


Days.allow({
	insert: function() {return false},
	update: function() {return false},
	remove: function() {return false}
});

Evaluations.allow({
	insert: function() {return true},
	update: function() {return true},
	remove: function() {return false}
});

Questions.allow({
	insert: function() {return false},
	update: function() {return false},
	remove: function() {return false}
});

SummaryStats.allow({
	insert: function() {return false},
	update: function() {return false},
	remove: function() {return false}
});

FeedbackMessages.allow({
	insert: function() {return true},
	update: function() {return false},
	remove: function() {return false}
});