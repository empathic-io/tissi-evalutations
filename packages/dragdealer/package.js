Package.describe({
	summary: "Dragdealer for Meteor"
});

Package.on_use(function(api) {
	api.add_files([
		"dist/src/dragdealer.css",
		"dist/src/dragdealer.js"
	], "client");
});
