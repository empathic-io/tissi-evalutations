Package.describe({
	summary: "Styles, JS and fonts of Open Mind Bootstrap-based template"
});

Package.on_use(function(api) {
	api.add_files([
		"dist/css/style.css",
		"dist/css/bootstrap.min.css",
		//"dist/css/font-awesome.min.css",
		"dist/js/bootstrap.min.js"
	], "client");

    api.add_files([
        // Icon fonts
        "dist/fonts/fontawesome-webfont.eot",
        "dist/fonts/fontawesome-webfont.svg",
        "dist/fonts/fontawesome-webfont.ttf",
        "dist/fonts/fontawesome-webfont.woff",
        "dist/fonts/glyphicons-halflings-regular.eot",
        "dist/fonts/glyphicons-halflings-regular.svg",
        "dist/fonts/glyphicons-halflings-regular.ttf",
        "dist/fonts/glyphicons-halflings-regular.woff",
    ],"client",{
        // undocumented hint : do not bundle those files along with with js/html resources
        isAsset:true
    });
});
