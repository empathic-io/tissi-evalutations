Evaluations.after.insert(function(userId, doc) {
	SummaryStatsApi.recomputeSummary(doc.target);
});

Evaluations.after.update(function(userId, doc) {
	SummaryStatsApi.recomputeSummary(doc.target);
});