AssetsApi = {
	_assetsFiles: {
		timeline: "timeline.json",
		questions: "questions.json"
	},

	loadAll: function(assetType) {
		return JSON.parse(Assets.getText(this._assetsFiles[assetType])) || [];
	}
};