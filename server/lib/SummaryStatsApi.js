SummaryStatsApi = {

	_getSchoolAverage: function(uid) {
		var evaluations = Evaluations.find({target: uid});
		if(evaluations.count() === 0) return 50;

		var total = 0;
		evaluations.forEach(function(evaluation) {
			total += evaluation.average;
		});

		return Math.round(total / evaluations.count());
	},

	_getEvaluationsCount: function(uid) {
		return Evaluations.find({target: uid}).count();
	},

	recomputeSummary: function(uid) {
		if(!uid) return;
		SummaryStats.upsert({target: uid}, {
			target: uid,
			schoolAverage: this._getSchoolAverage(uid),
			evaluationsCount: this._getEvaluationsCount(uid)
		});
	},

	recomputeAll: function() {
		SummaryStats.remove({});
		Days.find({}).forEach(function(day) {
			_.forEach(day.sections, function(section) {
				if(section.lectures) {
					_.forEach(section.lectures, function(lecture) {
						SummaryStatsApi.recomputeSummary(lecture.uid);
					});
				} else {
					SummaryStatsApi.recomputeSummary(section.uid);
				}
			});
		});

		/*Days.find({}).forEach(function(day) {
			_.forEach(day.getAllUids(), function(uid) {
				SummaryStatsApi.recomputeSummary(uid);
			});
		});*/
	}

};