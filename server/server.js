Meteor.startup(function() {
	Kadira.connect('eiz4Y6Fv2uoYaaGuo', '17abc710-02ed-48d3-886a-d2e03da28e0d');

	Days.remove({});
	AssetsApi.loadAll("timeline").forEach(function(doc) {
		Days.insert(doc);
	});

	Questions.remove({});
	AssetsApi.loadAll("questions").forEach(function(doc) {
		Questions.insert(doc);
	});

	SummaryStatsApi.recomputeAll();
});

Meteor.publish('all-days', function() {
	return Days.find({}, {reactive: false});
});

Meteor.publish('all-questions', function() {
	return Questions.find({}, {reactive: false});
});

Meteor.publish('evaluations-by-selector', function(selector) {
	return Evaluations.find(selector);
});

Meteor.publish('summary-stats-by-selector', function(selector) {
	return SummaryStats.find(selector);
});